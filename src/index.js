import { User } from './User.js'
import { Url } from './Url.js'

(function(window){
    'use strict';

    var erikaLibrary = () => {
        var _erikaLibraryObject = {};
    
        _erikaLibraryObject.initialize = () => {
            _erikaLibraryObject.user = new User()
            _erikaLibraryObject.url = new Url()
            delete _erikaLibraryObject.initialize
        }

        return _erikaLibraryObject;
    }

    if ( typeof(window.erika) === 'undefined' ) {
        window.erika = erikaLibrary()
    }
})(window)