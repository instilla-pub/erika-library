import uuidv4 from 'uuid/v4'
import * as cookies from 'js-cookie'
import * as md5 from 'md5'

export class User {

    constructor() {
        this._cid = cookies.get('instilla_cid')||null
        this._uid = cookies.get('instilla_uid')||null
        this._data = cookies.getJSON('instilla_user_data')||{}
        if (this.cid === null) {
            this.generateCid()
        }
    }

    // getters
    get cid() { return this._cid }
    get uid() { return this._uid }
    get data() { return this._data }

    // methods
    generateCid() {
        if (this.cid !== null) return null
        var cid = uuidv4().replace(/-/g, '')
        cookies.set('instilla_cid', cid, { expires: 3650 })
        this._cid = cid
        return this.cid
    }
    
    overrideCid(cid) {
        cookies.set('instilla_cid', cid, { expires: 3650 })
        this._cid = cid
        return this.cid
    }

    removeAttribute(key) {
        var data = this._data
        delete data[key]
        cookies.set('instilla_user_data', data)
        this._data = data
        return this.data
    }

    setAttribute(key, value) {
        var data = this._data
        data[key] = value
        cookies.set('instilla_user_data', data)
        this._data = data
        return this.data
    }

    setUid(uid) {
        if (!uid) return null
        cookies.set('instilla_uid', uid)
        this._uid = uid
        return this.uid 
    }

    setHashedUid(username, salt) {
        if (!username) return null
        if (salt) {
            username = salt+username
        }
        var uid = md5(btoa(username))
        this.setUid(uid)
        return this.uid
    }

    unset() {
        this._cid = null
        this._uid = null
        this._data = null
        cookies.remove('instilla_cid')
        cookies.remove('instilla_uid')
        cookies.remove('instilla_user_data')
        return true
    }
        
}