//import 'core-js/features/url-search-params';
//import * as psl from 'psl'

export class Url {

    constructor() {
        
    }

    // getters
    get base () { return document.location.origin }
    get location() { return window.location.href }
    get hostname () { return window.location.hostname }
    get itm() { return this.queryParameters.itm||null }
    get path () { return document.location.pathname }
    get protocol () { return document.location.protocol }
    get referrer () { return document.referrer }
    get queryParameters() {
        // from: https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
        var match
        var pl = /\+/g
        var search = /([^&=]+)=?([^&]*)/g
        var decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); }
        var query = window.location.search.substring(1)
        var parameters = {}
        while (match = search.exec(query)) {
           parameters[decode(match[1])] = decode(match[2])
        }
        return parameters
    }
}

    // // polyfilled getters
    // get domain () { return psl.parse(document.location.hostname).domain }
    // get sld () { return psl.parse(document.location.hostname).sld }
    // get subdomain () { return psl.parse(document.location.hostname).subdomain }
    // get tld () { return psl.parse(document.location.hostname).tld }
    // get queryParameters() { 
    //     var parameters = {}
    //     for (var pair of new URLSearchParams(document.location.search).entries() ) {
    //        parameters[pair[0]] = pair[1]; 
    //     }
    //     return parameters;
    // }